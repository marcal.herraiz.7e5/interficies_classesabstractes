
/*
    * NOM: Marçal Herraiz Prat
    * DATA:06/03/2023
    * TITOL: StudentWithTextGrade
 */

//Student{name='Mar', textGrade=FAILED}
//Student{name='Joan', textGrade=EXCELLENT}

data class Student(val name: String, val textGrade: TextGrade){
}

enum class TextGrade {
    FAILED,
    PASSED,
    GOOD,
    NOTABLE,
    EXCELLENT
}

fun main() {
    val mar = Student("Mar", TextGrade.FAILED)
    val joan = Student("Joan", TextGrade.EXCELLENT)
    println(mar)
    println(joan)
}