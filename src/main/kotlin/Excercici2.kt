
/*
    * NOM: Marçal Herraiz Prat
    * DATA:06/03/2023
    * TITOL: Rainbow
 */

interface Rainbow {
    val colors: List<String>
    fun isRainbowColor(color: String): Boolean {
        return colors.contains(color.toLowerCase())
    }
}

class SantMartiRainbow : Rainbow {
    override val colors = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
}

fun main() {
    val rainbow = SantMartiRainbow()
    val color = readLine()?.toLowerCase()
    if (color != null && rainbow.isRainbowColor(color)) {
        println("true")
    } else {
        println("false")
    }
}
