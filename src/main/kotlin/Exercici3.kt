
/*
    * NOM: Marçal Herraiz Prat
    * DATA:07/03/2023
    * TITOL: InstrumentSimulator
 */

abstract class Instrument {
    abstract fun makeSound(times: Int)
}

class Drump(private val tone: String) : Instrument() {
    override fun makeSound(times: Int) {
        for (i in 1..times) {
            when (tone) {
                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U" -> println("TUUUM")
            }
        }
    }
}

class Triangle(private val resonance: Int) : Instrument() {
    override fun makeSound(times: Int) {
        for (i in 1..times) {
            val sound = "T${"I".repeat(resonance)}NC"
            println(sound)
        }
    }
}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSound(2) // plays 2 times the sound
    }
}
