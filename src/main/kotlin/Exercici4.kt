
/*
    * NOM: Marçal Herraiz Prat
    * DATA:07/03/2023
    * TITOL: Quiz
 */

abstract class Question(val text: String) {
    abstract fun checkAnswer(answer: String): Boolean
}

class FreeTextQuestion(text: String, val correctAnswer: String) : Question(text) {
    override fun checkAnswer(answer: String): Boolean {
        return answer.trim().equals(correctAnswer, ignoreCase = true)
    }
}

class MultipleChoiceQuestion(text: String, val options: List<String>, val correctAnswer: Int) : Question(text) {
    override fun checkAnswer(answer: String): Boolean {
        val choice = answer.toIntOrNull()
        return choice != null && choice - 1 == correctAnswer
    }

    fun printOptions() {
        for (i in options.indices) {
            println("${i + 1}) ${options[i]}")
        }
    }
}

class Quiz(val questions: List<Question>) {
    fun run() {
        var correctAnswers = 0
        for (question in questions) {
            println(question.text)
            when (question) {
                is FreeTextQuestion -> {
                    val answer = readLine() ?: ""
                    if (question.checkAnswer(answer)) {
                        correctAnswers++
                    }
                }
                is MultipleChoiceQuestion -> {
                    question.printOptions()
                    val answer = readLine() ?: ""
                    if (question.checkAnswer(answer)) {
                        correctAnswers++
                    }
                }
            }
        }
        println("You got $correctAnswers out of ${questions.size} correct.")
    }
}

fun main() {
    val questions = listOf(
        FreeTextQuestion("What is the capital of Spain?", "Madrid"),
        MultipleChoiceQuestion(
            "What is the largest planet in the solar system?",
            listOf("Mercury", "Venus", "Earth", "Jupiter", "Saturn"),
            3
        ),
        FreeTextQuestion("What is the name of the largest ocean on Earth?", "Pacific"),
        MultipleChoiceQuestion(
            "What is the smallest country in the world by land area?",
            listOf("Monaco", "Vatican City", "Nauru", "San Marino", "Tuvalu"),
            1
        )
    )
    val quiz = Quiz(questions)
    quiz.run()
}
