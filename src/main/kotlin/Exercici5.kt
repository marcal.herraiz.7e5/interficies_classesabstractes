/*
    * NOM: Marçal Herraiz Prat
    * DATA:08/03/2023
    * TITOL: AutonomousCarPrototype
 */

enum class Direction {
    FRONT, LEFT, RIGHT
}

interface CarSensors {
    fun isThereSomethingAt(direction: Direction): Boolean
    fun go(direction: Direction)
    fun stop()
}

class AutonomousCar(private val sensors: CarSensors) : CarSensors by sensors {

    fun doNextNSteps(n: Int) {
        repeat(n) {
            val hasObstacleFront = isThereSomethingAt(Direction.FRONT)
            val hasObstacleLeft = isThereSomethingAt(Direction.LEFT)
            val hasObstacleRight = isThereSomethingAt(Direction.RIGHT)

            if (!hasObstacleFront) {
                go(Direction.FRONT)
            } else if (!hasObstacleRight) {
                go(Direction.RIGHT)
            } else if (!hasObstacleLeft) {
                go(Direction.LEFT)
            } else {
                stop()
            }
        }
    }
}

class CarSensorsImpl : CarSensors {

    // Mapa de obstáculos para simular el entorno del coche
    private val obstacles = mutableMapOf(
        Direction.FRONT to false,
        Direction.LEFT to false,
        Direction.RIGHT to false
    )

    override fun isThereSomethingAt(direction: Direction): Boolean {
        // Devuelve true si hay un obstáculo en la dirección indicada
        return obstacles[direction] ?: false
    }

    override fun go(direction: Direction) {
        // Simula el movimiento del coche y actualiza el mapa de obstáculos
        obstacles[Direction.FRONT] = false
        obstacles[Direction.LEFT] = false
        obstacles[Direction.RIGHT] = false

        when (direction) {
            Direction.FRONT -> {
                println("Moving forward")            }
            Direction.LEFT -> {
                println("Moving left")
            }
            Direction.RIGHT -> {
                println("Moving right")
            }
        }
    }

    override fun stop() {
        // Detiene el coche y actualiza el mapa de obstáculos
        obstacles[Direction.FRONT] = true
        obstacles[Direction.LEFT] = true
        obstacles[Direction.RIGHT] = true
        println("Stopped")
    }
}


fun main() {
    val n = readln().toInt()
    val carSensors = CarSensorsImpl()
    val car = AutonomousCar(carSensors)
    car.doNextNSteps(n)
}
